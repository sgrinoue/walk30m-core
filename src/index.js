import GeoUtils from "./GeoUtil"
import CalculationService from "./CalculationService"

export default class {
  constructor() {
    this._task = null
    this._service = new CalculationService(global.google, { verbose: false })
  }

  _createSplined(path) {
    const initialHalf = path.slice(0, Math.floor(path.length / 2))
    const splined = GeoUtils.spline(path.concat(initialHalf))
    const { values: trimmed } = splined.reduce(
      ({ done, values }, e) => {
        if (done) return { done, values }

        if (path[1].equals(e)) {
          if (values.length > 0) return { done: true, values }
          return { done, values: [e] }
        }
        if (values.length > 0) return { done, values: values.concat(e) }
        return { done, values }
      },
      { done: false, values: [] }
    )

    return trimmed
  }

  _calcDistance(p1, p2) {
    const [p1Lat, p2Lat, p1Lng, p2Lng] = [
      p1.lat(),
      p2.lat(),
      p1.lng(),
      p2.lng()
    ].map(value => (Math.PI * value) / 180)

    return (
      6370 *
      1000 *
      Math.acos(
        Math.sin(p1Lat) * Math.sin(p2Lat) +
          Math.cos(p1Lat) * Math.cos(p2Lat) * Math.cos(p1Lng - p2Lng)
      )
    )
  }

  start(
    origin,
    time,
    { mode, anglePerStep, avoidFerrirs, avoidTolls, avoidHighways }
  ) {
    this._task = this._service.start({
      origin,
      time,
      mode,
      anglePerStep,
      avoidFerrirs,
      avoidTolls,
      avoidHighways
    })
  }

  stop() {
    this._service.stop()
    this._task = null
  }

  addListener(eventType, handler) {
    switch (eventType) {
      case "progress":
        return this._service.addListener(
          "progress",
          (progress, vertex, path) => {
            if (path.length < 1) return

            handler(
              this._createSplined(path.concat([this._task.config.origin])),
              progress
            )
          }
        )
      case "complete":
        return this._service.addListener("complete", () => {
          const path = this._task.getGoals()
          const origin = this._task.config.origin
          const splinedPath = this._createSplined(path)
          const {
            point: farthestPoint,
            dist: farthestDist
          } = splinedPath.reduce(
            ({ point, dist }, pathElement) => {
              const distance = this._calcDistance(origin, pathElement)

              return point === null || dist < distance
                ? { point: pathElement, dist: distance }
                : { point, dist }
            },
            { point: null, dist: 0 }
          )

          handler(splinedPath, farthestPoint, farthestDist)
        })
      case "error":
        return this._service.addListener("error", handler)
      default:
        throw new Error("Unknown event", eventType)
    }
  }

  removeListener(listener) {
    this._service.removeListener(listener)
  }
}
