import _ from "lodash"
import Calculation from "./Calculation"
import GeoUtil from "./GeoUtil"

function CalculationService(google, { verbose }) {
  const me = this

  me._google = google
  me._listeners = []
  me._verbose = !!verbose

  me.directionsService = new google.maps.DirectionsService()
  me.isRunning = false

  me.addListener("error", _.bind(me.stop, me))
}

CalculationService.prototype._log = function(...args) {
  if (this._verbose) console.log(...args)
}

CalculationService.prototype.addListener = function(event, listener) {
  const entry = [event, listener]

  this._listeners.push(entry)
  return entry
}

CalculationService.prototype.removeListener = function(listener) {
  this._listeners = this._listeners.filter(l => l !== listener)
}

CalculationService.prototype._notify = function(eventType, ...args) {
  this._listeners
    .filter(([e]) => eventType === e)
    .forEach(([, fn]) => fn(...args))
}

CalculationService.prototype.calcRoute = function(
  task,
  dest,
  apiCallStats,
  callback,
  successiveRateLimitCount
) {
  const me = this

  let config

  let options

  if (!task) {
    return
  }

  config = task.config
  options = _.pick(
    config,
    "origin",
    "avoidFerries",
    "avoidTolls",
    "avoidHighways"
  )

  me._notify("request", dest)
  me._log(
    "CalculationService: do directions request",
    options.origin.toString(),
    dest.toString()
  )

  setTimeout(() => {
    me.directionsService.route(
      _.defaults(
        {
          destination: dest,
          travelMode: me._google.maps.TravelMode[config.mode]
        },
        options
      ),
      (res, status) => {
        const updatedApiCallStats = _.set(
          apiCallStats,
          [status],
          (apiCallStats[status] || 0) + 1
        )

        if (status !== me._google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
          callback(res, status, updatedApiCallStats)
        } else if (successiveRateLimitCount < 3) {
          _.delay(() => {
            me.calcRoute(
              task,
              dest,
              updatedApiCallStats,
              callback,
              ++successiveRateLimitCount
            )
          }, successiveRateLimitCount * 2000)
        } else {
          me._notify("warn", "FREQUENT_OVER_QUERY_LIMIT")
          me.calcRoute(task, dest, updatedApiCallStats, callback, 0)
        }
      }
    )
  }, 250)
}

CalculationService.prototype.calcNext = function(task, dest, isNarrowing) {
  const me = this

  function tryDispatch(res, status, apiCallStats) {
    if (me.isPausing) {
      _.delay(() => {
        tryDispatch(res, status, apiCallStats)
      }, 100)
    } else {
      me.dispatch(
        task,
        res,
        status,
        {
          destination: dest,
          isNarrowing
        },
        apiCallStats
      )
    }
  }

  me.calcRoute(
    task,
    dest,
    {},
    (res, status, apiCallStats) => {
      if (me.isRunning) {
        tryDispatch(res, status, apiCallStats)
      }
    },
    0
  )
}

CalculationService.prototype.stopMonitorVelocity = function(task) {
  if (task.timeout) {
    window.clearInterval(task.timeout)
    delete task.timeout
  }
}

CalculationService.prototype.startMonitorVelocity = function(
  task,
  initialDelay,
  interval
) {
  const me = this

  if (task.timeout) {
    me._log("CalculationService: already monitoring", task)
    return
  }

  task.timeConsumed = 0
  _.delay(() => {
    task.timeout = window.setInterval(() => {
      if (!me.isRunning) {
        me.stopMonitorVelocity(task)
        return
      }

      const velocity = task.getVelocity()

      me._log("CalculationService: current velocity: ", velocity)

      if (velocity < 1) {
        me._notify("warn", "CALCULATION_IS_GETTING_SLOWER")
      }
    }, interval)
  }, initialDelay)
}

CalculationService.prototype.start = function(request) {
  const me = this

  const task = new Calculation(request)

  const origin = request.origin

  const travelVelocity =
    request.mode === me._google.maps.TravelMode.DRIVING ? 50 : 4

  const guessedDistance = travelVelocity * 1000 * (request.time / 60 / 60)

  me.startMonitorVelocity(task, 30000, 1000)

  me.currentTask = task
  me.isPausing = false
  me.isRunning = true
  me.calcNext(
    task,
    new me._google.maps.LatLng(
      origin.lat(),
      origin.lng() + GeoUtil.meterToLng(guessedDistance, origin.lat())
    )
  )

  me._notify("start", task)
  return task
}

CalculationService.prototype.walkToFillSecondsAlong = function(sec, wayPoints) {
  function getPartial(step, ratio) {
    let len = 0

    const lat_lngs = []

    const limited_length =
      _.reduce(
        step.lat_lngs,
        (passed, v, idx, arr) =>
          idx === arr.length - 1
            ? passed
            : passed + GeoUtil.distance(v, arr[idx + 1]),
        0
      ) * ratio

    _.each(step.lat_lngs, (pos, idx, arr) => {
      if (
        idx === 0 ||
        (len += GeoUtil.distance(pos, arr[idx - 1])) <= limited_length
      ) {
        lat_lngs.push(pos)
        return true
      }
      return false
    })
    return lat_lngs
  }

  return _.reduce(
    wayPoints,
    (passed, step) => {
      let next_accum

      if (passed && passed.end_flag) {
        return passed
      }
      next_accum = (passed ? passed.accum : 0) + step.duration.value
      if (next_accum > sec) {
        step.lat_lngs = getPartial(
          step,
          1 - (next_accum - sec) / step.duration.value
        )
      }

      step.accum = next_accum
      step.end_flag = next_accum > sec
      step.concat_path = ((passed && passed.concat_path) || []).concat(
        step.lat_lngs
      )
      return step
    },
    null
  )
}

CalculationService.prototype.stop = function() {
  const me = this

  if (me.isRunning) {
    me.isRunning = false
    me.stopMonitorVelocity(me.currentTask)
    delete me.currentTask
  }
}

CalculationService.prototype.pause = function() {
  const me = this

  me.isPausing = true
  me.currentTask.pauseStarted = new Date()
}

CalculationService.prototype.resume = function() {
  const me = this

  const task = me.currentTask

  me.isPausing = false
  if (task && task.pauseStarted) {
    task.pauseTime = task.pauseTime || 0
    task.pauseTime += new Date() - task.pauseStarted
  }
}

CalculationService.prototype.willBeCompletedWith = function(task, wayPoint) {
  return task.isComplete(task.getGoals().concat(wayPoint))
}

CalculationService.prototype.getAdvancedOne = function(task, p1, p2) {
  const origin = task.config.origin

  return GeoUtil.calcAngle(origin, p1) <= GeoUtil.calcAngle(origin, p2)
    ? p2
    : p1
}

CalculationService.prototype.dispatch = function(
  task,
  response,
  status,
  request,
  apiCallStats
) {
  let leg

  let lastWayPoint

  let step

  const me = this

  const dest = request.destination

  const sec = task.config.time

  const center = task.config.origin

  const anglePerStep = task.config.anglePerStep

  let nextDest

  if (task !== me.currentTask) return

  task.apiCallStats = _.mergeWith(
    task.apiCallStats || {},
    apiCallStats || {},
    (a, b) => (a || 0) + (b || 0)
  )

  if (status === me._google.maps.DirectionsStatus.ZERO_RESULTS) {
    nextDest = GeoUtil.rotate(center, GeoUtil.divide(center, dest, 0.8), 5)
    me.calcNext(task, nextDest, true)
  } else if (status !== me._google.maps.DirectionsStatus.OK) {
    me._notify("error", status)
  } else {
    leg = response.routes[0].legs[0]

    if (leg.duration.value > sec * 1.2) {
      nextDest = GeoUtil.divide(center, dest, 0.8)
      me.calcNext(task, nextDest, true)
    } else if (!request.isNarrowing && leg.duration.value < sec) {
      nextDest = GeoUtil.divide(
        center,
        dest,
        Math.max(1.1, sec / Math.max(leg.duration.value, 1))
      )

      if (me.getAdvancedOne(task, dest, nextDest) === dest) {
        nextDest = GeoUtil.rotate(center, nextDest, anglePerStep)
      }
      me.calcNext(task, nextDest)
    } else if ((step = me.walkToFillSecondsAlong(sec, leg.steps))) {
      lastWayPoint = step.lat_lngs[step.lat_lngs.length - 1]

      if (task.hasVisited(lastWayPoint)) {
        nextDest = GeoUtil.divide(center, GeoUtil.rotate(center, dest, 3), 0.8)
        me.calcNext(task, nextDest, true)
      } else if (me.willBeCompletedWith(task, lastWayPoint)) {
        me.isRunning = false
        me._notify("complete", task.vertices, task)
      } else {
        const vertex = {
          endLocation: lastWayPoint,
          step,
          directionResult: response
        }

        task.vertices.push(vertex)

        me._notify("progress", task.getProgress(), vertex, task.getGoals())

        nextDest = GeoUtil.rotate(
          center,
          me.getAdvancedOne(task, lastWayPoint, dest),
          anglePerStep
        )

        me.calcNext(task, nextDest)
      }
    } else {
      me._notify("error")
    }
  }
}

export default CalculationService
